from django.contrib import admin
from django.urls import path
from django.urls import include

from .views import FrontPageBaseTemplateView

urlpatterns = [
    path(
        'admin/',
        admin.site.urls
    ),
    path(
        '',
        FrontPageBaseTemplateView.as_view(),
        name='front_page'
    ),
    path(
        'tracker/',
        include('tracker.urls', namespace='tracker')
    ),
]
