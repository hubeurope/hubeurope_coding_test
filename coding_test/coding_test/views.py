from django.views.generic import TemplateView


class FrontPageBaseTemplateView(TemplateView):
    """
    Template for front page. View will contains tracking functions
    and details about the HubEurope.
    """

    template_name = 'frontpage.html'
